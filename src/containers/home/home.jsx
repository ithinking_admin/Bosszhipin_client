import React, {Component} from 'react';
import { TabBar } from 'antd-mobile';
import {connect} from 'react-redux';

import List from "./List";
import MessageList from "./messageList";
import MyInfo from "./myInfo";

import io from "socket.io-client";
import { addAllMsg } from "../../redux/actions";


// // 连接服务器 , 得到代表连接的 socket 对象 111.67.205.4:4000
let socket = io('ws://127.0.0.1:4000');
console.log(socket);

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'redTab',
            msgArray : [],
        };
    }

    componentWillMount() {

        // //监听来自服务器的消息
        // let a = this.props._id.toString();
        // setTimeout( ()=> {
        //     console.log('home页面延迟1收，home连接打开');
        //     // let socket = io('ws://127.0.0.1:4000');
        //     console.log(socket);
        //     socket.on(a,(obj) => {
        //         console.log('收到消息 : ', obj);
        //         console.log(this);
        //         let allmsg = this.state.msgArray;
        //         allmsg.push(obj);
        //         let proObj = this.state;
        //         proObj.msgArray = allmsg;
        //         this.setState(proObj);
        //         console.log(this.state);
        //         setTimeout(function(){
        //             document.documentElement.scrollIntoView({
        //                 behavior: "smooth",
        //                 block: "end",
        //                 inline: "nearest"
        //             });
        //         },2000);
        //
        //         //修改redux中的聊天记录
        //         let searchResult = false;
        //         let AllMsg = this.props.AllMsg;
        //         AllMsg.forEach(function(ele){
        //             if(ele.with === obj.from){
        //                 ele.msg.push(obj);
        //                 searchResult = true;
        //             }
        //         });
        //         if (searchResult === false){
        //             let a = {};
        //             a.with = obj.from;
        //             a.msg = [];
        //             a.msg.push(obj);
        //             console.log(a);
        //             AllMsg.push(a);
        //         }
        //
        //         this.props.addAllMsg({AllMsg});
        //         console.log(this.props);
        //     });
        // },100);
        //
        // console.log(this.props);
    }

    componentWillUnmount() {
        // console.log('home页面unmount执行，连接close');
        // // let socket = io('ws://127.0.0.1:4000');
        // socket.disconnect();
    }

    renderContent(pageText) {
        return (
           <div>
               <List> </List>
           </div>
        );
    }

    render() {

        return (
            <div style={{ position: 'fixed', height: '100%', width: '100%', top: 0 }}>
                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#33A3F4"
                    barTintColor="white"
                >
                    <TabBar.Item
                        title="列表"
                        key="List"
                        icon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(https://zos.alipayobjects.com/rmsportal/psUFoAMjkCcjqtUCNPxB.svg) center center /  21px 21px no-repeat' }}
                        />
                        }
                        selectedIcon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(https://zos.alipayobjects.com/rmsportal/IIRLrXXrFAhXVdhMWgUI.svg) center center /  21px 21px no-repeat' }}
                        />
                        }
                        selected={this.state.selectedTab === 'blueTab'}
                        // badge={1}
                        onPress={() => {
                            this.setState({
                                selectedTab: 'blueTab',
                            });
                        }}
                        data-seed="logId"
                    >
                        <List> </List>
                    </TabBar.Item>



                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' +require('../../assets/images/chat.png')+ ')' + 'center center /  21px 21px no-repeat' }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' +require('../../assets/images/chats.png')+ ')' + 'center center /  21px 21px no-repeat' }}
                            />
                        }
                        title="消息"
                        key="Messages"
                        // badge={'new'}
                        selected={this.state.selectedTab === 'redTab'}
                        onPress={() => {
                            this.setState({
                                selectedTab: 'redTab',
                            });
                        }}
                        data-seed="logId1"
                    >
                       <MessageList> </MessageList>
                    </TabBar.Item>



                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' +require('../../assets/images/my.png')+ ')' + 'center center /  21px 21px no-repeat' }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' +require('../../assets/images/mys.png')+ ')' + 'center center /  21px 21px no-repeat' }}
                            />
                        }
                        title="我的"
                        key="My"
                        // dot
                        selected={this.state.selectedTab === 'greenTab'}
                        onPress={() => {
                            this.setState({
                                selectedTab: 'greenTab',
                            });
                        }}
                    >
                       <MyInfo> </MyInfo>
                    </TabBar.Item>

                </TabBar>
            </div>
        );
    }
}

export default connect(
    state => state.user,
    {addAllMsg}
)(Home)