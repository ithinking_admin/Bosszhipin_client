import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavBar, Icon, Button,Modal,Card, WingBlank, WhiteSpace} from 'antd-mobile';
import QueueAnim from 'rc-queue-anim';
// import {Redirect} from 'react-router-dom'

import {logOut} from '../../redux/actions';

import '../../socketio/test';
import './style.less';

const alert = Modal.alert;

class MyInfo extends Component {

    state = {
        username: '',
        password: '',
        flag:0,
    };

    // componentWillMount() {
    //     let type = this.props.type;
    //     if(type === 'laoban'){
    //         type = 'dashen';
    //     }
    //     else{
    //         type = 'laoban';
    //     }
    //     this.props.allList({mold: type});
    //     console.log(this.props);
    // }
    //
    // tochat = (ele,index) =>{
    //     console.log('tochat事件触发了一次',ele);
    //     this.props.addtochat({chattingIndex:index});
    //     window.location.hash = '#/dashentochat';
    // };

    logOut = () =>{
        this.props.logOut();
        window.location.hash = '#/';
    };

    render() {

        const listType = '消息列表';

        return (
            <div>

                <NavBar
                    mode="dark"
                    leftContent='我的信息'
                    rightContent={[ <Icon key="1" type="ellipsis"/> ]}
                    style = {{position:'fixed',width:'100%',zIndex:'100'}}
                >
                </NavBar>
                <div style={{height:'45px',width:'100%'}}> </div>


                <img className={'headerImg'}
                    src={ require('../../assets/images/' + this.props.header + '.png') }
                    alt="我的头像" />
                <p className={'name'}>{this.props.username}</p>
                {this.props.type === 'laoban'?
                    <p className={'company'}>阿里巴巴</p> : null}

                <p className={'infoTitle'}>
                    {this.props.type === 'laoban'? '招聘信息 :':'求职信息 :'}
                </p>

                <div className={'info'}>
                    <p> {this.props.type === 'laoban' ? `职位 : ${this.props.post}` :
                        `求职 : ${this.props.post}`}</p>
                    {this.props.type === 'laoban' ? `薪资 ：${this.props.salary}` : null}
                    <p className={'infoBody'}> {this.props.type === 'laoban' ? `要求 : ${this.props.info}` :
                        `简介 : ${this.props.info}`}</p>
                </div>

                <WhiteSpace size="xl" />
                <Button type="warning"
                        style={{width:'90%',margin:'0 auto'}}
                        onClick={() =>
                            alert('提示', '确定要退出登录吗？', [
                                { text: '否', onPress: () => console.log('用户取消点击') },
                                { text: '是', onPress: () => this.logOut() },
                            ])
                        }
                >
                    退出登录
                </Button>


            </div>
        )
    }
}

export default connect(
    state => state.user,
    {logOut}
)(MyInfo)