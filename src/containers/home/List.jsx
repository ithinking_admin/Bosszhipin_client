import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavBar, Icon, Card, WingBlank, WhiteSpace} from 'antd-mobile';
import QueueAnim from 'rc-queue-anim';
import {Redirect} from 'react-router-dom'

import {allList,addtochat} from '../../redux/actions';

import '../../socketio/test';
import '../../assets/css/index.less';

class List extends Component {

    state = {
        username: '',
        password: '',
        flag:0,
    };

    componentWillMount() {
        let type = this.props.type;
        if(type === 'laoban'){
            type = 'dashen';
        }
        else{
            type = 'laoban';
        }
        this.props.allList({mold: type});
        console.log(this.props);
    }

    tochat = (listIndex) =>{
        this.props.addtochat({chattingIndex:listIndex});
        window.location.hash = '#/dashentochat';
    };

    render() {

        let listType = (this.props.type === 'dashen' ? '老板列表' : '大神列表');

            return (
                <div>

                    <NavBar
                        mode="dark"
                        leftContent={listType}
                        rightContent={<Icon key="1" type="ellipsis"/>}
                        style = {{position:'fixed',width:'100%',zIndex:'100'}}
                    >{this.props.username}</NavBar>
                    <div style={{height:'45px',width:'100%'}}> </div>


                    <div>
                        {this.props.data? this.props.data.map((ele,index) => (
                        <QueueAnim duration={700}  type='scale' key={index}>
                            <div key={index} onClick={(e) => this.tochat(index, e)}>
                                <WingBlank size="lg">
                                    <WhiteSpace size="lg"/>
                                    <Card>
                                        <Card.Header
                                            title={ele.username}
                                            thumb={ require('../../assets/images/' + ele.header + '.png')}
                                            extra={<span>
                                            <span>离线&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <span>{ele.company}</span></span>}
                                        />
                                        <Card.Body>
                                            <div>{ele.info}</div>
                                        </Card.Body>
                                        {listType === '大神列表' ?
                                            <Card.Footer content={'求职 : ' + ele.post} extra={<div> </div>}/> :
                                            <Card.Footer content={'招聘 : ' + ele.post} extra={<div>{'薪资' + ele.salary}</div>}/>
                                        }
                                    </Card>
                                    <WhiteSpace size="sm"/>

                                </WingBlank>
                            </div>
                        </QueueAnim>
                        )):null}
                    </div>


                </div>
            )
        }
}

export default connect(
    state => state.user,
    {allList,addtochat}
)(List)