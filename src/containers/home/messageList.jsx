import React, {Component} from 'react';
import {Icon, List, NavBar, Badge, WingBlank, WhiteSpace, Card} from 'antd-mobile';
import QueueAnim from "rc-queue-anim";
import {connect} from 'react-redux';
import {addtochat} from "../../redux/actions"; //

const Item = List.Item;
const Brief = Item.Brief;

class MessageList extends Component {
    state = {
        disabled: false,
    };

    componentWillMount() {
        console.log('MessageList将要挂载');
        console.log(this.props);
    }

    tochat = (listIndex) =>{
        console.log('tochat事件触发了一次,listIndex为: ',listIndex);
        this.props.addtochat({chattingIndex:listIndex});
        window.location.hash = '#/dashentochat';
    };

    render() {
        return (<div>

            <NavBar
                mode="dark"
                leftContent='消息'
                rightContent={<Icon key="1" type="ellipsis"/>}
                style = {{position:'fixed',width:'100%',zIndex:'100'}}
            >{this.props.username}</NavBar>
            <div style={{height:'45px',width:'100%'}}> </div>


            <div>
                { this.props.AllMsg.length ?

                   <List>
                    { this.props.AllMsg.map((ele,index) => (
                        <Item key={index} onClick={(e) => this.tochat(ele.withListIndex, e)}
                              extra={<Badge text={0} overflowCount={55} />}
                              align="middle"
                              thumb={ require('../../assets/images/' + ele.header + '.png')}
                              error
                        >
                            {ele.withName}
                            <Brief>{ele.msg[ele.msg.length - 1].content}</Brief>
                        </Item>))
                    }
                </List>

                :

                <div >
                    <img className={'noMessage'}
                         src={ require('../../assets/images/noMessage.jpg') }
                         alt="暂无消息"/>
                    <p className={'noMessageTip'}>
                        {this.props.type ==='dashen' ? '暂无消息，赶快与老板联系吧 ！'
                            : '暂无消息，赶快与大神联系吧 !'}
                    </p>
                </div>

                }
            </div>


        </div>);
    }
}

export default connect(
    state => state.user,
    {addtochat}
)(MessageList)
