import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavBar, Icon, Card, WingBlank, WhiteSpace} from 'antd-mobile';
import QueueAnim from 'rc-queue-anim';
import {Redirect} from 'react-router-dom'

import {allList,addtochat} from '../../redux/actions';

import '../../socketio/test';
import '../../assets/css/index.less';

class LaobanList extends Component {

    state = {
        username: '',
        password: '',
        flag:0,
    };

    componentWillMount() {
        this.props.allList({mold: 'dashen'});
        console.log(this.props);
    }

    tochat = (ele,index) =>{
        console.log('tochat事件触发了一次',ele);
        this.props.addtochat({chattingIndex:index});
        window.location.hash = '#/dashentochat';
    };

    render() {

        const listType = this.props.type === 'dashen' ? '老板列表' : '大神列表';

            return (
                <div>

                    <NavBar
                        mode="dark"
                        leftContent={listType}
                        rightContent={[
                            <Icon key="0" type="search" style={{marginRight: '16px'}}/>,
                            <Icon key="1" type="ellipsis"/>,
                        ]}
                        style = {{position:'fixed',width:'100%',zIndex:'100'}}
                    >{this.props.username}</NavBar>
                    <div style={{height:'45px',width:'100%'}}> </div>


                    <div > {this.props.data? this.props.data.map((ele,index) => (
                        <QueueAnim duration={700}  type='scale' key={index}>
                            <div key={index} onClick={(e) => this.tochat(ele,index, e)}>
                                <WingBlank size="lg">
                                    <WhiteSpace size="lg"/>
                                    <Card>
                                        <Card.Header
                                            title={ele.username}
                                            thumb={ require('../../assets/images/' + ele.header + '.png')}
                                            extra={<span>
                                            <span>在线&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <span>{ele.company}</span></span>}
                                        />
                                        <Card.Body>
                                            <div>{ele.info}</div>
                                        </Card.Body>
                                        <Card.Footer content={'招聘 : ' + ele.post} extra={<div>{'薪资' + ele.salary}</div>}/>
                                    </Card>
                                    <WhiteSpace size="lg"/>
                                </WingBlank>
                            </div>
                        </QueueAnim>
                    )):null}



                    </div>


                </div>
            )
        }
}

export default connect(
    state => state.user,
    {allList,addtochat}
)(LaobanList)