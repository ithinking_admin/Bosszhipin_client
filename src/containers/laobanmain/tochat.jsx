import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavBar, Icon, SearchBar, List,Toast} from 'antd-mobile';
import QueueAnim from 'rc-queue-anim';

import io from 'socket.io-client';

import '../../assets/css/index.less';
import './style.less'

import image from '../../assets/images/头像1.png';

const Item = List.Item;


// // 连接服务器 , 得到代表连接的 socket 对象 111.67.205.4:4000
const socket = io('ws://127.0.0.1:4000');
console.log(socket);


class Tochat extends Component {


    constructor(props) {
        super(props);
        this.state = {
            myid:1,
            toindex:2,
            msg : '',
            msgArray : [],
        };
    }


    UNSAFE_componentWillMount() {

        let toindex = this.props.location.state.index;
        console.log(this.props.data[toindex]);
        let ostate = this.state;
        ostate.toindex = toindex;
        ostate.myid = this.props._id;
        this.setState(ostate);

        let a = this.props.data[this.state.toindex]._id.toString();
        socket.on(a,(obj) => {
            console.log('收到消息 : ', obj);
            console.log(this);
            let allmsg = this.state.msgArray;
            allmsg.push(obj);
            let proObj = this.state;
            proObj.msgArray = allmsg;
            this.setState(proObj);
            console.log(this.state);
            setTimeout(function(){
                document.documentElement.scrollIntoView({
                    behavior: "smooth",
                    block: "end",
                    inline: "nearest"
                });
            },100);
        });
    }


    submit = (obj) => {
        socket.emit('send', obj);
        let allmsg = this.state.msgArray;
        allmsg.push(obj);
        let proObj = this.state;
        proObj.msgArray = allmsg;
        this.setState(proObj);
        console.log(this.state);
        setTimeout(function(){
            document.documentElement.scrollIntoView({
                behavior: "smooth",
                block: "end",
                inline: "nearest"
            });
        },100);

        console.log(this.props);

    };

    successToast = () => {
        Toast.success('发送完成,对方已成功收到消息!', 1.2);
    };


    render() {


        return (
            <div>


                <div className='iTopFixed'>
                    <NavBar icon={<Icon type="left"/>}
                            onLeftClick={() => console.log('onLeftClick')}
                            rightContent={[<Icon key="1" type="ellipsis"/>,]}
                    >用户1</NavBar>
                </div>
                <div style={{width:"100%",height:'50px'}}> </div>

                <List> {this.state.msgArray.map((ele, index) => (
                    <QueueAnim  className="queue-simple">
                        <div style={{width:'100%',lineHeight:'40px',fontSize:'18px'}} key={index}>
                            <img className={ele.from === this.state.myid ? 'rightImg':'leftImg'}
                                 src={image}/>
                            <div className={ele.from === this.state.myid ? 'rightText':'leftText'}>
                                {ele.content}</div>
                        </div>
                    </QueueAnim>
                ))}
                </List>



                <div style={{width:"100%",height:'50px'}}> </div>
                <div className='iBottomFixed'>
                    <SearchBar
                        // value={this.state.inputValue}
                        onCancel={value => {
                            this.submit({content:value,from:this.state.myid,
                                to:this.props.data[this.state.toindex]._id})
                        }}
                        onSubmit={value => {
                            this.submit({content:value,from:this.state.myid,
                                to:this.props.data[this.state.toindex]._id})
                        }}
                        cancelText='发送'/>
                </div>

            </div>

        )
    }
}

export default connect(
    state => state.user,
    // {allList}
)(Tochat)